#!/usr/bin/make -f

NULL =
PKD   = $(word 1,$(abspath $(dir $(MAKEFILE_LIST))))
PKG   = $(word 2,$(shell dpkg-parsechangelog -l$(PKD)/changelog | grep ^Source))
UVER  = $(shell dpkg-parsechangelog -l$(PKD)/changelog | perl -ne 'print $$1 if m{^Version:\s+(?:\d+:)?(\d.*)(?:\-\d+.*)};')
DTYPE = +dfsg1
VER  ?= $(subst $(DTYPE),,$(UVER))

include /usr/share/cdbs/1/rules/debhelper.mk
include /usr/share/cdbs/1/class/autotools.mk
include /usr/share/cdbs/1/rules/utils.mk

DEB_DH_ICONS_ARGS := --no-act

DEB_CONFIGURE_SCRIPT := ./autogen.sh

install/mate-icon-theme-faenza::
	# workaround upstream bug https://github.com/mate-desktop/mate-icon-theme-faenza/issues/19, remove
	# broken symlinks with missing targets...
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/16/address-book-new.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/16/stock_new-address-book.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/22/address-book-new.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/22/offline.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/22/online.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/22/stock_new-address-book.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/24/address-book-new.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/24/offline.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/24/online.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/24/stock_new-address-book.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/32/address-book-new.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/32/stock_new-address-book.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/48/address-book-new.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/48/stock_new-address-book.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/64/address-book-new.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/64/stock_new-address-book.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/96/address-book-new.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/96/stock_new-address-book.png
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/scalable/address-book-new.svg
	rm debian/mate-icon-theme-faenza/usr/share/icons/matefaenzagray/actions/scalable/stock_new-address-book.svg

	find debian/mate-icon-theme-faenza/usr/share/icons/matefaenza/ -type f | while read file; do chmod a-x "$$file"; done

get-orig-source: $(PKG)_$(VER)$(DTYPE).orig.tar.xz $(info I: $(PKG)_$(VER)$(DTYPE))
	@

$(PKG)_$(VER)$(DTYPE).orig.tar.xz:
	@echo "# Downloading..."
	uscan --noconf --verbose --rename --destdir=$(CURDIR) --check-dirname-level=0 --force-download --download-version $(VER) $(PKD)
	$(if $(wildcard $(PKG)-$(VER)),$(error $(PKG)-$(VER) exist, aborting..))
	@echo "# Extracting..."
	mkdir $(PKG)-$(VER) \
	    && tar -xf $(PKG)_$(VER).orig.tar.* --directory $(PKG)-$(VER) --strip-components 1 \
	    || $(RM) -r $(PKG)-$(VER)
	@echo "# Cleaning-up..."
	cd $(PKG)-$(VER) \
	    && $(RM) -r -v \
	    matefaenza/*/*/*flash* \
	    matefaenza/*/*/*pdf* \
	    matefaenza/*/*/*alien-arena* \
	    matefaenza/*/*/*ccc_large* \
	    matefaenza/*/*/*picasa* \
	    $(NULL)
	$(RM) -v $(PKG)_$(VER).orig.tar.*
	@echo "# Packing..."
	find -L "$(PKG)-$(VER)" -xdev -type f -print | sort \
	    | XZ_OPT="-6v" tar -caf "../$(PKG)_$(VER)$(DTYPE).orig.tar.xz" -T- --owner=root --group=root --mode=a+rX \
	    && $(RM) -r "$(PKG)-$(VER)"
